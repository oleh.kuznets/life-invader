import { HomeComponent } from './home/home.component';
import { MotoComponent } from './moto/moto.component';
import { PricingComponent } from './pricing/pricing.component';
import { CustomersComponent } from './customers/customers.component';
import { ServicesComponent } from './services/services.component';
import { SourcesComponent } from './sources/sources.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { SubscribeComponent } from './subscribe/subscribe.component';

export const HomeComponents: any[] = [
    HomeComponent,
    MotoComponent,
    PricingComponent,
    CustomersComponent,
    ServicesComponent,
    SourcesComponent,
    HowItWorksComponent,
    SubscribeComponent,
];

export * from './home/home.component';
export * from './moto/moto.component';
export * from './pricing/pricing.component';
export * from './customers/customers.component';
export * from './services/services.component';
export * from './sources/sources.component';
export * from './how-it-works/how-it-works.component';
export * from './subscribe/subscribe.component';
