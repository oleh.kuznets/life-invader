import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import * as components from './components';

@NgModule({
    declarations: [
        ...components.HomeComponents,
    ],
    imports: [
        CommonModule
    ],
    providers: [],
    exports: [
        ...components.HomeComponents
    ],
    bootstrap: []
})
export class HomeModule { }
