import { AbstractHttpClient } from '../utils/abstract-http.client';
import { HttpConfig } from '../utils/http-config.model';
import { Injectable } from '@angular/core';
import { RepositoryConfig } from '../utils/repositories.constant';

@Injectable()
export class ProfileRepository extends AbstractHttpClient {
    config: HttpConfig = RepositoryConfig.profile;
}
