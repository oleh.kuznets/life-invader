export interface HttpConfig {
    url: string;
    options?: object;
}

export enum API_RESPONSE_STATUS {
    success = 'success',
    error = 'error',
}

export interface ApiResponseModel<T> {
    response_datetime: string;
    status: API_RESPONSE_STATUS;
    data: T;
}
