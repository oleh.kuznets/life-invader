import { AppState } from 'src/app/store';
import { catchError, finalize, map } from 'rxjs/operators';
import { ExtendedRequestOptions, RequestOptions } from '../interfaces/request.interface';
import { HttpClient } from '@angular/common/http';
import { HttpClientAction } from '../../../store/actions/http.actions';
import { HttpConfig } from './http-config.model';
import { HttpService } from '../services/http.service';
import { Injectable, Injector } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Store } from '@ngrx/store';

declare let EXPLT_ENV_VARS;

let uid = 0;
export class HttpClientRequest {
    public id: any = uid++;
    public isSuccess = false;
    public isFail = false;
    public isFinalized = false;

    constructor(httClientRequestId?: any) {
        if (httClientRequestId) {
            this.id = httClientRequestId;
        }
    }

    isCancelRequest() {
        return this.isSuccess === false && this.isFail === false;
    }
}

@Injectable()
export class AbstractHttpClient {
    public config: HttpConfig;
    protected httpClient: HttpClient;
    protected store: Store<AppState>;
    protected httpService: HttpService;
    private apiBaseUrl: string;

    constructor(protected injector: Injector) {
        this.httpClient = injector.get(HttpClient);
        this.store = injector.get<any>(Store);
        this.httpService = injector.get<any>(HttpService);
        this.apiBaseUrl = `${window.location.protocol}//${EXPLT_ENV_VARS.XLUI_API_URL}`;
    }

    getAll<U>(options?: RequestOptions): Observable<any> {
        return this.sendRequest<U[]>('get', this.config.url, options);
    }

    get<U>(id: number | string, options?: RequestOptions): Observable<U> {
        return this.sendRequest<U>('get', `${this.config.url}/${id}`, options);
    }

    post<T, U>(body: T | null, options?: RequestOptions): Observable<U> {
        return this.sendRequest<U>('post', this.config.url, {
            ...options,
            body,
        });
    }

    patch<T, U>(id: number | string, body: T | null, options?: RequestOptions): Observable<U> {
        const url = id ? `${this.config.url}/${id}` : this.config.url;
        return this.sendRequest<U>('patch', url, { ...options, body });
    }

    put<T, U>(id: number | string, body: T | null, options?: RequestOptions): Observable<U> {
        return this.sendRequest<U>('put', `${this.config.url}/${id}`, {
            ...options,
            body,
        });
    }

    delete<U>(id: number | string, options?: RequestOptions): Observable<any> {
        return this.sendRequest<U>('delete', `${this.config.url}/${id}`, options);
    }

    deleteAll<U>(options?: RequestOptions): Observable<any> {
        return this.sendRequest<U[]>('delete', this.config.url, options);
    }

    sendRequest<U>(method: string, url: string, options?: ExtendedRequestOptions): Observable<any> {
        const httpClientRequest = new HttpClientRequest();
        this.httpService.requests.push(httpClientRequest);
        return this.httpClient
            .request<U>(
                method,
                `${this.apiBaseUrl}${url}`,
                this.config.options ? { ...options, ...this.config.options } : options
            )
            .pipe(
                map(response => {
                    httpClientRequest.isSuccess = true;
                    return response;
                }),
                catchError((error: any) => {
                    httpClientRequest.isFail = true;
                    return throwError(error);
                }),
                finalize(() => {
                    if (httpClientRequest.isCancelRequest()) {
                        this.store.dispatch({
                            type: HttpClientAction.REQUEST_CANCELED,
                        });
                    }
                    httpClientRequest.isFinalized = true;
                })
            );
    }
}
