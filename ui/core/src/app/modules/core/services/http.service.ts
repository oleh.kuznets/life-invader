import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store';


@Injectable()
export class HttpService {
    public requests = [];
    constructor(public store: Store<AppState>) {
        setInterval(() => {
            if (this.requests.length < 1) {
                return;
            }
            const result = _.every(this.requests, request => {
                return request.isFinalized || request.isSuccess || request.isFail;
            });
            const anyCanceled = _.some(
                this.requests,
                request => request.isFinalized && !request.isSuccess && !request.isFail
            );
            if (result) {
                this.requests = [];
                if (anyCanceled) {
                    console.log('...canceled');
                }
            }
        }, 10000);
    }

    getRequest(id) {
        return _.find(this.requests, { id });
    }
}
