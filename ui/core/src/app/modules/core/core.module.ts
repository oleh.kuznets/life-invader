import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRepository } from './repositories/header.repository';
import { HttpService } from './services/http.service';

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ],
    providers: [
        HttpService,
        ProfileRepository
    ],
    exports: [],
    bootstrap: []
})

export class CoreModule { }
