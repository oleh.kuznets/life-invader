import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Shared components
import * as components from '../components';

import { ThemeService } from '../services/theme.service';


@NgModule({
    declarations: [
        ...components.CommonComponents,
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        ButtonsModule.forRoot(),
        BsDropdownModule.forRoot()
    ],
    providers: [
        ThemeService
    ],
    exports: [
        ...components.CommonComponents
    ],
    bootstrap: []
})
export class SharedModule { }
