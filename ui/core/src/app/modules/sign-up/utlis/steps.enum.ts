export enum SignUpSteps {
  Terms = 'terms',
  Owner = 'owner',
  Platform = 'platform',
  Image = 'image',
  Security = 'security',
  Success = 'success',
}
