import { StepInterface } from './step.interface';
import { SignUpSteps } from './steps.enum';

export const STEPS_LIST: StepInterface[] = [
    {
        name: 'terms',
        component: SignUpSteps.Terms,
        active: true,
        label: SignUpSteps.Terms,
        index: '1'
    },
    {
        name: 'owner',
        component: SignUpSteps.Owner,
        active: false,
        label: SignUpSteps.Owner,
        index: '2'
    },
    {
        name: 'platform',
        component: SignUpSteps.Platform,
        active: false,
        label: SignUpSteps.Platform,
        index: '3'
    },
    {
        name: 'image',
        component: SignUpSteps.Image,
        active: false,
        label: SignUpSteps.Image,
        index: '4'
    },
    {
        name: 'security',
        component: SignUpSteps.Security,
        active: false,
        label: SignUpSteps.Security,
        index: '5'
    },
    {
        name: 'success',
        component: SignUpSteps.Success,
        active: false,
        label: SignUpSteps.Success,
        index: '6'
    }
];
