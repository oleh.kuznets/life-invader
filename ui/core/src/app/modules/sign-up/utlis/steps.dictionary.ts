import { OwnerInfoComponent } from '../components/owner-info/owner-info.component';
import { PlatformImageComponent } from '../components/platform-image/platform-image.component';
import { PlatformSecurityComponent } from '../components/platform-security/platform-security.component';
import { SignUpSteps } from './steps.enum';
import { SuccessComponent } from '../components/success/success.component';
import { TermsAndPoliciesComponent } from '../components/terms-and-policies/terms-and-policies.component';
import { PlatformInfoComponent } from '../components/platform-info/platform-info.component';

export const Steps = {
    [SignUpSteps.Terms]: TermsAndPoliciesComponent,
    [SignUpSteps.Owner]: OwnerInfoComponent,
    [SignUpSteps.Platform]: PlatformInfoComponent,
    [SignUpSteps.Image]: PlatformImageComponent,
    [SignUpSteps.Security]: PlatformSecurityComponent,
    [SignUpSteps.Success]: SuccessComponent,
};
