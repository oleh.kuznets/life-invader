export interface StepInterface {
  name: string;
  component: string;
  active: boolean;
  label: string;
  index: string;
}
