import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

// Components
import * as components from './components';
import { SharedModule } from '../shared.module';

@NgModule({
    declarations: [
        ...components.SignUpComponents
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        ButtonsModule.forRoot(),
        SharedModule
    ],
    providers: [],
    exports: [
        ...components.SignUpComponents
    ],
    bootstrap: []
})
export class SignUpModule { }
