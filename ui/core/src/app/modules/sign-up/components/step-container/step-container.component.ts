import * as fromStore from '../../../../store';
import {
    Component,
    OnDestroy,
    OnInit,
    Type
    } from '@angular/core';
import { select } from '@ngrx/store';
import { StepModel } from '../../models/step.model';
import { Steps } from '../../utlis/steps.dictionary';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/internal/Subscription';
import { SignUpSteps } from '../../utlis/steps.enum';

@Component({
    selector: 'app-step-container',
    templateUrl: './step-container.component.html',
    styleUrls: ['./step-container.component.scss']
})
export class StepContainerComponent implements OnInit, OnDestroy {

    component: Type<any>;
    subscriptions: Subscription[] = [];

    constructor(
        private store: Store<fromStore.AppState>
    ) { }

    ngOnInit() {
        this.subscriptions.push(
            this.store
                .pipe(select(fromStore.getAppState))
                .subscribe((state) => {
                    if (!state.signUp.steps) {
                        return;
                    }

                    const activeStep = state.signUp.steps.find(step => step.active);
                    this.component = this.changeContainerType(activeStep);
                })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    private changeContainerType(step): Type<any> {
        return Steps[step.component];
    }

}
