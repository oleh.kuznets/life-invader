import { Component, OnInit } from '@angular/core';
import * as fromStore from '../../../../store';
import { Store, select } from '@ngrx/store';
import { faChevronRight, faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-next-step',
    templateUrl: './next-step.component.html',
    styleUrls: ['./next-step.component.scss']
})
export class NextStepComponent implements OnInit {

    public nextStep: any;
    public faDynamicIcon = faChevronRight;

    constructor(
        private store: Store<fromStore.AppState>
    ) { }

    ngOnInit(): void {
        this.getButtonData();
    }

    public onClick(): void {
        this.store.dispatch({ type: fromStore.SignUpActions.SET_ACTIVE_NEXT_STEP });
        this.getButtonData();
    }

    private getButtonData(): void {
        this.store
            .pipe(select(fromStore.getAppState))
            .subscribe((state) => {
                this.nextStep = state.signUp.nextStep;
                this.getIcon(this.nextStep);
            });
    }

    private getIcon(state): void {
        this.faDynamicIcon = state.finish === true ? faCheck : faChevronRight;
    }

}
