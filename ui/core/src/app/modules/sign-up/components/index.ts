import { OwnerInfoComponent } from './owner-info/owner-info.component';
import { NextStepComponent } from './next-step/next-step.component';
import { PreviousStepComponent } from './previous-step/previous-step.component';
import { PlatformImageComponent } from './platform-image/platform-image.component';
import { PlatformSecurityComponent } from './platform-security/platform-security.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { StepContainerComponent } from './step-container/step-container.component';
import { SuccessComponent } from './success/success.component';
import { TermsAndPoliciesComponent } from './terms-and-policies/terms-and-policies.component';
import { PlatformInfoComponent } from './platform-info/platform-info.component';

export const SignUpComponents: any[] = [
    SignUpComponent,
    OwnerInfoComponent,
    PlatformImageComponent,
    PlatformSecurityComponent,
    SuccessComponent,
    TermsAndPoliciesComponent,
    NextStepComponent,
    PreviousStepComponent,
    StepContainerComponent,
    PlatformInfoComponent
];

export * from './owner-info/owner-info.component';
export * from './platform-image/platform-image.component';
export * from './platform-security/platform-security.component';
export * from './sign-up/sign-up.component';
export * from './success/success.component';
export * from './terms-and-policies/terms-and-policies.component';
export * from './next-step/next-step.component';
export * from './previous-step/previous-step.component';
export * from './step-container/step-container.component';
export * from './platform-info/platform-info.component';
