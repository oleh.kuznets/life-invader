import * as fromStore from '../../../../store';
import { SignUpActions } from 'src/app/store';
import { Component, OnInit } from '@angular/core';
import { StepInterface } from '../../utlis/step.interface';
import { STEPS_LIST } from '../../utlis/steps.list.constant';
import { Store, select } from '@ngrx/store';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

    public steps = STEPS_LIST as StepInterface[];
    public stepsState: StepInterface[];

    constructor(
        private store: Store<fromStore.AppState>
    ) { }

    public ngOnInit(): void {
        this.store.dispatch({ type: SignUpActions.SHOW_NEXT_STEP });
        this.setSteps();
        this.getStepState();
    }

    private getStepState(): void {
        this.store
            .pipe(select(fromStore.getAppState))
            .subscribe((state) => {
                this.stepsState = state.signUp.steps;
            });
    }

    private setSteps(): void {
        this.store.dispatch({
            type: fromStore.SignUpActions.SET_STEPS,
            payload: this.steps,
        });
    }

}
