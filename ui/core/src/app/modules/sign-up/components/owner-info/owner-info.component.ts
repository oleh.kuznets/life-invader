import * as fromStore from '../../../../store';
import isEmpty from 'lodash/isEmpty';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { User } from 'src/app/interfaces/user.interface';

@Component({
    selector: 'app-owner-info',
    templateUrl: './owner-info.component.html',
    styleUrls: ['./owner-info.component.scss']
})
export class OwnerInfoComponent implements OnInit, OnDestroy {

    public owner: User;
    public ownerFullName: string;

    public ownerForm: FormGroup;
    public firstName: FormControl;
    public secondName: FormControl;
    public email: FormControl;
    public phone: FormControl;
    public company: FormControl;
    public isValidFirstName: boolean;
    public isValidSecondName: boolean;
    public isValidEmail: boolean;
    public isValidCompany: boolean;
    public formChanges = null;
    public existedData = null;

    private emailValidationPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    private isNextStepEnabled = false;
    private destroy$ = new Subject();
    private formData: any;

    constructor(
        private store: Store<fromStore.AppState>,
        private formBuilder: FormBuilder
    ) {
        this.owner = {
            firstName: 'Not Defined',
            secondName: 'Not Defined',
            email: 'Not Defined',
            company: 'Not Defined',
            phone: 'Not Defined',
        };

        this.ownerFullName = 'Not Defined';
    }

    public ngOnInit(): void {
        this.getPreviousData();
        this.getInitialState();
        this.initForm();
        this.getFormChanges();
        this.getStepData();
    }

    public dropForm(): void {
        this.ownerForm.reset({
            firstName: '',
            secondName: '',
            email: '',
            phone: '',
            company: ''
        });

        this.formData = null;
        this.setDataToStore(this.formData);
    }

    private getPreviousData(): void {
        this.store
            .pipe(takeUntil(this.destroy$), select(fromStore.getAppState))
            .subscribe((state) => {
                this.existedData = state.signUp.newUserObject.newUserObject;
                if (this.existedData) {
                    this.owner.firstName = this.existedData.firstName;
                    this.owner.secondName = this.existedData.secondName;
                    this.owner.email = this.existedData.email;
                    this.owner.company = this.existedData.company;
                    this.owner.phone = this.existedData.phone;

                    this.ownerFullName = `${this.owner.firstName || ''} ${this.owner.secondName || ''}`;
                }
            });
    }

    private getInitialState(): void {
        this.store.dispatch({ type: fromStore.SignUpActions.DISABLE_NEXT_STEP });
        this.store.dispatch({ type: fromStore.SignUpActions.SHOW_PREV_STEP });
    }

    private getFormChanges(): void {
        this.ownerForm.valueChanges.subscribe((user) => {
            this.formChanges = user;
            this.owner.firstName = user.firstName || this.existedData.firstName || '';
            this.owner.secondName = user.secondName || this.existedData && this.existedData.secondName || '';
            this.owner.email = user.email.trim() || this.existedData && this.existedData.email || 'Not Defined';
            this.owner.company = user.company.trim() || this.existedData && this.existedData.company || 'Not Defined';
            this.owner.phone = user.phone.trim() || this.existedData && this.existedData.phone || 'Not Defined';

            this.ownerFullName = `${user.firstName || ''} ${user.secondName || ''}`.trim() || 'Not Defined';

            this.validateControls();
            this.validateSummary();
        });
    }

    public validateField(): void {
        this.validateControls();
    }

    private validateControls(): void {
        const formControls = this.ownerForm.controls;

        this.isValidFirstName = formControls.firstName.invalid && formControls.firstName.touched;
        this.isValidSecondName = formControls.secondName.invalid && formControls.secondName.touched;
        this.isValidEmail = formControls.email.invalid && formControls.email.touched;
        this.isValidCompany = formControls.company.invalid && formControls.company.touched;
    }

    private getStepData(): void {
        this.store
            .pipe(takeUntil(this.destroy$), select(fromStore.getAppState))
            .subscribe((state) => {
                this.isNextStepEnabled = state.signUp.nextStep.enabled;
            });
    }

    private setDataToStore(data): void {
        this.store.dispatch({
            type: fromStore.SignUpActions.SET_DATA,
            payload: {
                property: 'newUserObject',
                value: data
            },
        });
    }

    private validateSummary(): void {
        if (this.ownerForm.valid && this.isNextStepEnabled === false) {
            this.store.dispatch({ type: fromStore.SignUpActions.ENABLE_NEXT_STEP });
        } else if (!this.ownerForm.valid && this.isNextStepEnabled === true) {
            // this.store.dispatch({ type: fromStore.SignUpActions.DISABLE_NEXT_STEP });
        }
    }

    private initForm(): void {

        this.ownerForm = this.formBuilder.group(
            {
                firstName: ['', [Validators.required]],
                secondName: ['', [Validators.required]],
                email: ['', [Validators.required, Validators.pattern(this.emailValidationPattern)]],
                company: ['', [Validators.required]],
                phone: '',
            },
            {
                validator: [],
            }
        );
    }

    public ngOnDestroy(): void {
        this.setDataToStore(this.owner);
        this.destroy$.next();
        this.destroy$.complete();
    }

}
