import { Component, OnInit } from '@angular/core';
import * as fromStore from '../../../../store';
import { Store, select } from '@ngrx/store';
import { take } from 'rxjs/operators';

@Component({
    selector: 'app-terms-and-policies',
    templateUrl: './terms-and-policies.component.html',
    styleUrls: ['./terms-and-policies.component.scss']
})
export class TermsAndPoliciesComponent implements OnInit {

    public checkboxStatus = false;

    constructor(
        private store: Store<fromStore.AppState>
    ) { }

    public ngOnInit(): void {
        this.store.dispatch({ type: fromStore.SignUpActions.HIDE_PREV_STEP });
        this.store.dispatch({
            type: fromStore.SignUpActions.SET_STEP_AS_NEXT,
        });

        this.getTermsConfirmationStatus();
    }

    public triggerCheckbox(status: boolean): void {
        this.checkTermsConfirmation(status);
        this.store.dispatch({
            type: fromStore.SignUpActions.SET_DATA,
            payload: {
                property: 'terms',
                value: status
            },
        });
    }

    private checkTermsConfirmation(status: boolean): void {
        status
            ? this.store.dispatch({ type: fromStore.SignUpActions.ENABLE_NEXT_STEP })
            : this.store.dispatch({ type: fromStore.SignUpActions.DISABLE_NEXT_STEP });
    }

    private getTermsConfirmationStatus(): void {
        this.store
            .pipe(take(1), select(fromStore.getAppState))
            .subscribe((state) => {
                this.checkboxStatus = state.signUp.newUserObject.terms;
                this.checkTermsConfirmation(this.checkboxStatus);
            });
    }

}
