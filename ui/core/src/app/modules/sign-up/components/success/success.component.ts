import { Component, OnInit } from '@angular/core';
import * as fromStore from '../../../../store';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-success',
    templateUrl: './success.component.html',
    styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

    constructor(
        private store: Store<fromStore.AppState>
    ) { }

    ngOnInit(): void {
        this.store.dispatch({ type: fromStore.SignUpActions.HIDE_PREV_STEP });
        this.store.dispatch({ type: fromStore.SignUpActions.HIDE_NEXT_STEP });
    }

}
