import { Component, OnInit } from '@angular/core';
import * as fromStore from '../../../../store';
import { Store, select } from '@ngrx/store';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-previous-step',
    templateUrl: './previous-step.component.html',
    styleUrls: ['./previous-step.component.scss']
})
export class PreviousStepComponent implements OnInit {

    public previousStep: any;
    public faChevronLeft = faChevronLeft;

    constructor(
        private store: Store<fromStore.AppState>
    ) { }

    ngOnInit(): void {
        this.store
            .pipe(select(fromStore.getAppState))
            .subscribe(state => (this.previousStep = state.signUp.previousStep));
    }

    public onClick(): void {
        this.store.dispatch({
            type: fromStore.SignUpActions.SET_ACTIVE_PREVIOUS_STEP,
        });
    }

}
