import { Component, OnInit } from '@angular/core';
import * as fromStore from '../../../../store';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-platform-image',
    templateUrl: './platform-image.component.html',
    styleUrls: ['./platform-image.component.scss']
})
export class PlatformImageComponent implements OnInit {

    constructor(
        private store: Store<fromStore.AppState>
    ) { }

    ngOnInit(): void {
        this.store.dispatch({ type: fromStore.SignUpActions.SET_STEP_AS_NEXT });
    }

}
