import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformImageComponent } from './platform-image.component';

describe('PlatformImageComponent', () => {
  let component: PlatformImageComponent;
  let fixture: ComponentFixture<PlatformImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
