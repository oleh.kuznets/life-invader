import { Component, OnInit } from '@angular/core';
import * as fromStore from '../../../../store';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-platform-security',
    templateUrl: './platform-security.component.html',
    styleUrls: ['./platform-security.component.scss']
})
export class PlatformSecurityComponent implements OnInit {

    constructor(
        private store: Store<fromStore.AppState>
    ) { }

    ngOnInit(): void {
        this.store.dispatch({ type: fromStore.SignUpActions.SET_STEP_AS_FINISH });
        this.store.dispatch({ type: fromStore.SignUpActions.SHOW_NEXT_STEP });
    }

}
