export class StepModel {
  constructor(
      public name: string,
      public component: string,
      public active: boolean,
      public label: string
  ) {}
}
