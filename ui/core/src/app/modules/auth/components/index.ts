import { SignInComponent } from './sign-in/sign-in.component';

export const AuthComponents: any[] = [
    SignInComponent
];

export * from './sign-in/sign-in.component';
