import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Store } from '@ngrx/store';
import { UserActions } from 'src/app/store/actions';
import * as fromStore from '../../../../store';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

    public logInForm: FormGroup;
    public firstName: FormControl;
    public secondName: FormControl;
    public email: FormControl;
    public bio: FormControl;

    constructor(
        public modalRef: BsModalRef,
        private store: Store<fromStore.AppState>,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit(): void {
        this.initForm();
    }

    private initForm(): void {
        this.logInForm = this.formBuilder.group(
            {
                firstName: ['', [Validators.required]],
                secondName: ['', [Validators.required]],
                email: ['', [Validators.required]],
                bio: [''],
            },
            {
                validator: [],
            }
        );
    }

    public onSubmit(): void {
        this.modalRef.hide();
        this.store.dispatch({ type: UserActions.LOG_IN });
    }

}
