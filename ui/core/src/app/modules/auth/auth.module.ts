import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// Components
import * as components from './components';

@NgModule({
    declarations: [
        ...components.AuthComponents
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FontAwesomeModule,
    ],
    providers: [],
    exports: [
        ...components.AuthComponents
    ],
    bootstrap: []
})
export class AuthModule { }
