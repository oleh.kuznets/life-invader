import { Component, OnInit } from '@angular/core';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-sign-up-content',
    templateUrl: './sign-up-content.component.html',
    styleUrls: ['./sign-up-content.component.scss']
})
export class SignUpContentComponent implements OnInit {

    public faUserPlus = faUserPlus;

    constructor() { }

    ngOnInit(): void {
    }

}
