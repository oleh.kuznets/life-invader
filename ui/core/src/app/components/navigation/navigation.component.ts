import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

    public isDarkTheme: boolean;

    constructor(
        private themeService: ThemeService
    ) { }

    public ngOnInit(): void {
    }

    public toggleTheme(event: MouseEvent): void {
        this.isDarkTheme = this.themeService.getThemeState(event);
    }

}
