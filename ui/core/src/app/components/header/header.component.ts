import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeaderExclusions } from 'src/app/constants/header-exclusions.constant';
import { Router, RoutesRecognized } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

    public showHeaderContent = true;
    public alternativeTitle: string;
    public isDarkTheme: boolean;

    private destroyStream = new Subject();

    constructor(
        private router: Router,
        private themeService: ThemeService
    ) { }

    public navigate() {
        this.router.navigate(['./home']);
    }

    public ngOnInit(): void {
        this.themeService.getAppThemeState()
            .pipe(takeUntil(this.destroyStream))
            .subscribe(item => this.isDarkTheme = item);
        this.modifyHeaderTemplate();
    }

    private modifyHeaderTemplate(): void {
        this.router.events.pipe(takeUntil(this.destroyStream)).subscribe(data => {
            if (data instanceof RoutesRecognized) {
                this.getHeaderVisibility(data);
            }
        });
    }

    private getHeaderVisibility(data): void {
        const foundedExclusion = HeaderExclusions.find(exclusion => data.url.includes(exclusion.url));

        if (foundedExclusion) {
            this.showHeaderContent = foundedExclusion.url ? false : true;
            this.alternativeTitle = foundedExclusion.title;
        }
    }

    public ngOnDestroy(): void {
        this.destroyStream.next();
        this.destroyStream.complete();
    }

}
