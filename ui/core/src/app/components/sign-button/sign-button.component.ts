import * as fromStore from '../../store';
import { BsModalService } from 'ngx-bootstrap';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { faUser, faUsersCog } from '@fortawesome/free-solid-svg-icons';
import { select, Store } from '@ngrx/store';
import { SignInComponent } from 'src/app/modules/auth/components/sign-in/sign-in.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserActions } from '../../store';

@Component({
    selector: 'app-sign-button',
    templateUrl: './sign-button.component.html',
    styleUrls: ['./sign-button.component.scss']
})
export class SignButtonComponent implements OnInit, OnDestroy {

    public faUsersCog = faUsersCog;
    public faUser = faUser;
    public isLoggedIn = false;

    private destroy$ = new Subject();

    constructor(
        private store: Store<fromStore.AppState>,
        private bsModalService: BsModalService
    ) { }

    ngOnInit(): void {
        this.initUserState();
    }


    public openLogInModal(): void {
        const config = {
            ignoreBackdropClick: true,
            keyboard: false,
            class: 'log-in'
        };
        this.bsModalService.show(SignInComponent, config);
    }

    public toggleState(): void {
        !this.isLoggedIn
            ? this.store.dispatch({ type: UserActions.LOG_IN })
            : this.store.dispatch({ type: UserActions.LOG_OUT });
        this.initUserState();
    }

    private initUserState(): void {
        this.store.pipe(
            select(fromStore.appState),
            takeUntil(this.destroy$))
            .subscribe(state => {
                this.isLoggedIn = state.user.loggedIn;
            });
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

}
