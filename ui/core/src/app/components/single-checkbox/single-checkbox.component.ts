import { Component, OnInit, forwardRef, EventEmitter, Output, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-single-checkbox',
    templateUrl: './single-checkbox.component.html',
    styleUrls: ['./single-checkbox.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SingleCheckboxComponent),  // replace name as appropriate
            multi: true
        }
    ]
})
export class SingleCheckboxComponent implements OnInit {

    public checkboxStatus = 'false';
    public isActive = false;

    @Input() changedValue: boolean;
    @Output() triggerCheckbox: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor() {}

    public ngOnInit(): void {
        this.isActive = this.changedValue;
        this.checkboxStatus = this.changedValue === true ? 'true' : 'false';
    }

    public onClick(): void {
        const value = this.checkboxStatus === 'true' ? true : false;
        this.isActive = value;
        this.triggerCheckbox.emit(value);
    }

}
