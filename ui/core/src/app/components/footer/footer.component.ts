import { Component, OnDestroy, OnInit } from '@angular/core';
import { FooterExclusions } from 'src/app/constants/footer-exclusions.constant';
import { Router, RoutesRecognized } from '@angular/router';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {

    public showFooter = true;
    public isDarkTheme: boolean;

    private destroyStream = new Subject();

    constructor(
        private router: Router,
        private themeService: ThemeService
    ) { }

    public ngOnInit(): void {
        this.router.events.pipe(takeUntil(this.destroyStream)).subscribe(data => {
            if (data instanceof RoutesRecognized) {
                this.getFooterVisibility(data);
            }
        });

        this.themeService.getAppThemeState()
            .pipe(takeUntil(this.destroyStream))
            .subscribe(item => this.isDarkTheme = item);
    }

    private getFooterVisibility(data): void {
        const foundedExclusion = FooterExclusions.find(exclusion => data.url.includes(exclusion));
        this.showFooter = foundedExclusion ? false : true;
    }

    public ngOnDestroy(): void {
        this.destroyStream.next();
        this.destroyStream.complete();
    }

}
