import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LogoComponent } from './logo/logo.component';
import { SvgLogoComponent } from './svg-logo/svg-logo.component';
import { SignButtonComponent } from './sign-button/sign-button.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SignUpContentComponent } from './sign-up-content/sign-up-content.component';
import { SingleCheckboxComponent } from './single-checkbox/single-checkbox.component';

export const CommonComponents: any[] = [
    HeaderComponent,
    FooterComponent,
    SvgLogoComponent,
    LogoComponent,
    SignButtonComponent,
    NavigationComponent,
    SignUpContentComponent,
    SingleCheckboxComponent
];

export * from './header/header.component';
export * from './footer/footer.component';
export * from './logo/logo.component';
export * from './svg-logo/svg-logo.component';
export * from './sign-button/sign-button.component';
export * from './navigation/navigation.component';
export * from './sign-up-content/sign-up-content.component';
export * from './single-checkbox/single-checkbox.component';
