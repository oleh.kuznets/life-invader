import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { effects, metaReducers, reducers } from './store';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgModule } from '@angular/core';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Custom Modules
import { HomeModule } from './modules/home/home.module';
import { SharedModule } from './modules/shared.module';
import { CoreModule } from './modules/core/core.module';
import { AuthModule } from './modules/auth/auth.module';
import { SignUpModule } from './modules/sign-up/sign-up.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        SharedModule,
        CoreModule,
        HomeModule,
        AuthModule,
        SignUpModule,
        StoreModule.forRoot(reducers, { metaReducers }),
        EffectsModule.forRoot(effects),
        StoreDevtoolsModule.instrument({ maxAge: 25 }),
        StoreRouterConnectingModule.forRoot(),
        ModalModule.forRoot(),
        ButtonsModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
    ],
    providers: [],
    exports: [
        SharedModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
