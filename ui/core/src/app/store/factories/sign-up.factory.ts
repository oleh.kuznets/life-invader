import clone from 'lodash/fp/clone';
import cloneDeep from 'lodash/cloneDeep';
import findIndex from 'lodash/fp/findIndex';
import setWith from 'lodash/fp/setWith';
import { SignUpActions } from '../actions/sign-up.actions';

export const ReducerStateFactory = {

    [SignUpActions.CONFIRM_TERMS]: (state, action) => {
        return {
            ...state,
            newUserObject: {
                ...state.nextStep,
                terms: true,
            },
        };
    },

    [SignUpActions.UNCONFIRM_TERMS]: (state, action) => {
        return {
            ...state,
            newUserObject: {
                ...state.nextStep,
                terms: false,
            },
        };
    },

    [SignUpActions.ENABLE_NEXT_STEP]: (state, action) => {
        return {
            ...state,
            nextStep: {
                ...state.nextStep,
                enabled: true,
            },
        };
    },

    [SignUpActions.DISABLE_NEXT_STEP]: (state, action) => {
        return {
            ...state,
            nextStep: {
                ...state.nextStep,
                enabled: false,
            },
        };
    },

    [SignUpActions.SHOW_NEXT_STEP]: (state, action) => {
        return {
            ...state,
            nextStep: {
                ...state.nextStep,
                show: true,
            },
        };
    },

    [SignUpActions.HIDE_NEXT_STEP]: (state, action) => {
        return {
            ...state,
            nextStep: {
                ...state.nextStep,
                show: false,
            },
        };
    },

    [SignUpActions.SET_STEP_AS_FINISH]: (state, action) => {
        return {
            ...state,
            nextStep: {
                ...state.nextStep,
                finish: true,
                onFinishEvent: action.payload,
            },
        };
    },

    [SignUpActions.SET_STEP_AS_NEXT]: (state, action) => {
        return {
            ...state,
            nextStep: {
                ...state.nextStep,
                finish: false,
                onFinishEvent: null,
            },
        };
    },
    [SignUpActions.SHOW_PREV_STEP]: (state, action) => {
        return {
            ...state,
            previousStep: {
                ...state.previousStep,
                show: true,
                isZeroStep: false,
            },
        };
    },

    [SignUpActions.HIDE_PREV_STEP]: (state, action) => {
        return {
            ...state,
            previousStep: {
                ...state.previousStep,
                show: false,
            },
        };
    },

    [SignUpActions.SET_DATA]: (state, action) => {
        const { property, value } = action.payload;
        const user = state.newUserObject || {};

        const setIn = (path, newValue, obj) => setWith(clone, path, newValue, clone(obj));
        const newUserObject = setIn(property, value, user);

        return { ...state, newUserObject };
    },

    [SignUpActions.SET_STEPS]: (state, action) => {
        return {
            ...state,
            steps: action.payload,
        };
    },

    [SignUpActions.SET_ACTIVE_STEP]: (state, action) => {
        const updatedSteps = state.steps;
        const selected = findIndex('active', updatedSteps);
        const newSelected = findIndex({ name: action.payload }, updatedSteps);
        updatedSteps[selected].active = false;
        updatedSteps[newSelected].active = true;
        return {
            ...state,
            steps: [...updatedSteps],
        };
    },

    [SignUpActions.SET_ACTIVE_NEXT_STEP]: (state, action) => {
        const updatedSteps = cloneDeep(state.steps);
        const last = updatedSteps.length - 1;
        const selected = findIndex('active', updatedSteps);
        const next = selected === last ? last : selected + 1;
        updatedSteps[selected].active = false;
        updatedSteps[next].active = true;

        return {
            ...state,
            steps: [...updatedSteps],
        };
    },

    [SignUpActions.SET_ACTIVE_PREVIOUS_STEP]: (state, action) => {
        const updatedSteps = cloneDeep(state.steps);
        const selected = findIndex('active', updatedSteps);
        const prev = selected === 0 ? 0 : selected - 1;
        updatedSteps[selected].active = false;
        updatedSteps[prev].active = true;

        return {
            ...state,
            steps: [...updatedSteps],
        };
    },
};
