import { UserActions } from '../actions/user.actions';

export const ReducerStateFactory = {
    [UserActions.LOG_IN]: (state) => {
        return {
            ...state.loggedIn,
            loggedIn: true,
        };
    },

    [UserActions.LOG_OUT]: (state) => {
      return {
          ...state.loggedIn,
          loggedIn: false,
      };
  },
};


