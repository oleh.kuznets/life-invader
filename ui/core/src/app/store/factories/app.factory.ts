import { AppActions } from '../actions/app.actions';

export const ReducerStateFactory = {
    [AppActions.SET_DEFAULT_THEME]: (state) => {
        return {
            ...state,
            darkTheme: false
        };
    },

    [AppActions.SET_DARK_THEME]: (state) => {
        return {
            ...state,
            darkTheme: true
        };
    }
};

