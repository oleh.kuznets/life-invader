import { Action } from '@ngrx/store';
import { ReducerStateFactory } from '../factories/auth.factory';

export const initialState: any = null;

export function reducer(state: any = initialState, action: Action): any {
    return ReducerStateFactory[action.type] ? ReducerStateFactory[action.type](state, action) : state;
}
