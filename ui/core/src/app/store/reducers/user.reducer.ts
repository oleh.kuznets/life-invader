import { Action } from '@ngrx/store';
import { ReducerStateFactory } from '../factories/user.factory';

export const initialState: any = {
    loggedIn: false,
};

export function reducer(state: any = initialState, action: Action): any {
    return ReducerStateFactory[action.type] ? ReducerStateFactory[action.type](state, action) : state;
}
