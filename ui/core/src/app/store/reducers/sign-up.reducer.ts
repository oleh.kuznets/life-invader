import { Action } from '@ngrx/store';
import { ReducerStateFactory } from '../factories/sign-up.factory';

export const initialState: any = {
    nextStep: {
        enabled: false,
        show: true,
        finish: false,
    },
    previousStep: {
        show: false,
        isZeroStep: false,
    },
    steps: null,
    newUserObject: {
        terms: false
    }
};

export function reducer(state: any = initialState, action: Action): any {
    return ReducerStateFactory[action.type] ? ReducerStateFactory[action.type](state, action) : state;
}
