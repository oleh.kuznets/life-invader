import * as fromApp from './app.reducer';
import * as fromUser from './user.reducer';
import * as fromAuth from './auth.reducer';
import * as fromSignUp from './sign-up.reducer';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';

export interface AppState {
  router: RouterReducerState;
  app: any;
  user: any;
  signUp: any;
  auth: any;
}

export const reducers: ActionReducerMap<AppState> = {
  router: routerReducer,
  app: fromApp.reducer,
  user: fromUser.reducer,
  signUp: fromSignUp.reducer,
  auth: fromAuth.reducer
};

export const getAppState = (state: AppState) => state;

export const metaReducers: MetaReducer<AppState>[] = [];
