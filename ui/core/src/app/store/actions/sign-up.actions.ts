export enum SignUpActions {
  SET_STEPS                = `
    {
      app: core,
      action: SET_STEPS,
      description: create steps model
    }
  `,
  SET_ACTIVE_STEP          = '[App:Core | SET_ACTIVE_STEP] Set Active Step',
  SET_ACTIVE_NEXT_STEP     = '[App:Core | SET_ACTIVE_NEXT_STEP] Set Active Next Step',
  SET_ACTIVE_PREVIOUS_STEP = '[App:Core | SET_ACTIVE_PREVIOUS_STEP] Set Active Previous Step',
  CONFIRM_TERMS            = '[App:Core | CONFIRM_TERMS] Confirm Terms',
  UNCONFIRM_TERMS          = '[App:Core | UNCONFIRM_TERMS] Unconfirm Terms',
  ENABLE_NEXT_STEP         = '[App:Core | ENABLE_NEXT_STEP] Enable Next Step Button',
  DISABLE_NEXT_STEP        = '[App:Core | DISABLE_NEXT_STEP] Disable Next Step Button',
  SHOW_NEXT_STEP           = '[App:Core | SHOW_NEXT_STEP] Show Next Step Button',
  HIDE_NEXT_STEP           = '[App:Core | HIDE_NEXT_STEP] Hide Next Step Button',
  SET_STEP_AS_FINISH       = '[App:Core | SET_STEP_AS_FINISH] Set Step as Finish',
  SET_STEP_AS_NEXT         = '[App:Core | SET_STEP_AS_NEXT] Set Step as Next',
  SHOW_PREV_STEP           = '[App:Core | SHOW_PREV_STEP] Show Previous Step Button',
  HIDE_PREV_STEP           = '[App:Core | HIDE_PREV_STEP] Hide Previous Step Button',
  SET_DATA                 = '[App:Core | SET_DATA] Set Data',
}
