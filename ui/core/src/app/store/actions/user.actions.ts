export enum UserActions {
  LOG_IN  = '[App:Core | Action: LOG_IN] User Logged In',
  LOG_OUT = '[App:Core | Action: LOG_OUT] User Logged Out',
}
