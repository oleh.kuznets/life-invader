export enum AppActions {
    SET_DEFAULT_THEME = '[App:Core | SET_DEFAULT_THEME] Set default theme',
    SET_DARK_THEME = '[App:Core | SET_DARK_THEME] Set dark theme',
}
