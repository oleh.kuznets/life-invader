import { createSelector } from '@ngrx/store';
import { getAppState } from '../reducers';

export const appState = createSelector(
    getAppState,
    state => state
);
