import { Guidline } from './guidline.interface';
import { UsagePlan } from './usage-plan.interface';
import { User } from './user.interface';

export interface Platform {
  subdomain: string;
  name: string;
  company: string;
  createdDate: string;
  access: string;
  logo: string;
  wideLogo: string;
  status: string;
  guidlines: Guidline[];
  usagePlan: UsagePlan;
  owner: User;
  users: number;
}
