export interface User {
  type?: string;
  firstName: string;
  secondName: string;
  email: string;
  phone?: string;
  company: string;
}
