export interface UsagePlan {
  type: string;
  name: string;
  startDate: string;
  expirationDate: string;
  description: string;
  image: string;
  price: string;
  activated: boolean;
}
