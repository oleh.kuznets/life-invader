import { Injectable } from '@angular/core';
import * as fromStore from '../store';
import { select, Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { AppActions } from '../store';
import { Subject } from 'rxjs/internal/Subject';
import { Observable } from 'rxjs';

export const SHOW_COLLAPSED_ITEMS = 6;

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  public isDarkTheme = false;

  public themeState = new Subject<boolean>();

  constructor(
    private store: Store<fromStore.AppState>
  ) { }

  /**
   *
   * @param event : needs for trigger theme changing
   */
  public getThemeState(event = null): boolean {
    this.store.pipe(
      select(fromStore.appState),
      take(2))
      .subscribe(state => {
        this.isDarkTheme = state.app.darkTheme;
      });

    if (event) {
      this.toggleTheme(this.isDarkTheme);
    }


    this.addDarkThemeClass(this.isDarkTheme);
    this.updateAppThemeState(this.isDarkTheme);
    return this.isDarkTheme;
  }

  /**
   *
   * @param isDarkTheme : app theme state
   */
  public toggleTheme(isDarkTheme: boolean): void {
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
      this.store.dispatch({ type: AppActions.SET_DARK_THEME })
    }
    isDarkTheme
      ? this.store.dispatch({ type: AppActions.SET_DEFAULT_THEME })
      : this.store.dispatch({ type: AppActions.SET_DARK_THEME });
  }

  /**
   *
   * @param isDarkTheme : app theme state
   */
  public addDarkThemeClass(isDarkTheme: boolean): void {
    isDarkTheme
      ? document.querySelector<HTMLElement>('body').classList.add('dark-theme')
      : document.querySelector<HTMLElement>('body').classList.remove('dark-theme');
  }

  getAppThemeState(): Observable<boolean> {
    return this.themeState.asObservable();
  }

  updateAppThemeState(data: boolean): void {
    this.themeState.next(data);
  }
}
